﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool grounded;
    public GameObject player;
    public int playerSpeed = 10;
    private bool facingRight = false;
    public int playerJumpPower = 1250;
    private float moveX;
    bool canJump = true;
    public Canvas GameOverMenu;
    public Camera MainCamera;

    [FMODUnity.EventRef]
    public string jump_snd = "event:/Jump";   // string reference to the FMOD-authored Event named "Jump"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance jump_sndEv;      // Unity EventInstance name for Jump event that was created in FMOD

    [FMODUnity.EventRef]
    public string death = "event:/Death";   // string reference to the FMOD-authored Event named "Death"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance deathEv;      // Unity EventInstance name for Death event that was created in FMOD

    void Start() {

        GameOverMenu.enabled = false;
        
    }

    void Update()
    {
        PlayerMove();
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        
        if (other.gameObject.CompareTag("lava"))
        {
            // These lines should be pasted into the section where you want to cue/play the sound 
            deathEv = FMODUnity.RuntimeManager.CreateInstance(death);    // EventInstance is linked to the Event 
            deathEv.start();                                            // FINALLY... Play the sound!!

            Destroy(gameObject);
            GameOverMenu.enabled = true;

        }

    }

    void PlayerMove()

    {
        //CONTROLS
        moveX = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            Jump();
        }

        //PLAYER DIRECTION
        if (moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if (moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }
        //PHYSICS
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }

    void Jump() {
        //JUMPING CODE
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);

        // These lines should be pasted into the section where you want to cue/play the sound 
        jump_sndEv = FMODUnity.RuntimeManager.CreateInstance(jump_snd);    // EventInstance is linked to the Event 
        jump_sndEv.start();                                            // FINALLY... Play the sound!!

    }

    void FlipPlayer() {

        facingRight = !facingRight;
        Vector3 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;


    }
}