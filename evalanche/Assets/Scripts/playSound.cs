﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playSound : MonoBehaviour {

    [FMODUnity.EventRef]
    public string collision_snd = "event:/Jump";   // string reference to the FMOD-authored Event named "Jump"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance jump_sndEv;      // Unity EventInstance name for Jump event that was created in FMOD


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            return;

        }

        // These lines should be pasted into the section where you want to cue/play the sound 
        jump_sndEv = FMODUnity.RuntimeManager.CreateInstance(collision_snd);    // EventInstance is linked to the Event 
        jump_sndEv.start();                                            // FINALLY... Play the sound!!
        Debug.Log("sound");
    }

}
