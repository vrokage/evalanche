﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public Transform player;
    public Text scoreText;
    public static int score;
    public static int highscore;
    Text text;

    void Start () {

        text = GetComponent<Text> ();

        score = 0;

        highscore = PlayerPrefs.GetInt("highscore", highscore);
        text.text = highscore.ToString();
        
    }

    void Update () {

        scoreText.text = "Score : " + player.position.y.ToString("f0");

       
    }
}
