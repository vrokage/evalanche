﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grounded : MonoBehaviour {


    private PlayerMovement player;


	
	private void Start ()
    {

        player = GetComponentInParent<PlayerMovement>();

	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player.grounded = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        player.grounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        player.grounded = false;
    }
}
