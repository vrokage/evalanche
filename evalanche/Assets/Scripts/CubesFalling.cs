﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesFalling : MonoBehaviour {

    public GameObject Block1;
    public GameObject Block2;
    public GameObject Block3;
    public float delay = 0.10f;
    public GameObject [] Blocks;

	// Use this for initialization
	void Start () {

        InvokeRepeating("Spawn", delay, delay);

	}
	
	// Update is called once per frame
	void Spawn () {
        GameObject block = Blocks[Random.Range(0, Blocks.Length)];

        Instantiate(block, new Vector3(Random.Range(-18, 18), Camera.main.transform.position.y + 25, 0), Quaternion.identity);
     
    }





}
