﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class buttonManager : MonoBehaviour {

    public Canvas quitMenu;
    public Button startText;
    public Button exitText;
    public Button restart;
    public Button mainMenu;
    public bool isDead;
    public Canvas GameOverMenu;

    [FMODUnity.EventRef]
    public string MenuSelect1 = "event:/Menu_Select_1";   // string reference to the FMOD-authored Event named "Menu_Select_1"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance MenuSelect1Ev;      // Unity EventInstance name for Menu_Select_1 event that was created in FMOD

    [FMODUnity.EventRef]
    public string MenuSelect2 = "event:/Menu_Select_2";   // string reference to the FMOD-authored Event named "Menu_Select_2"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance MenuSelect2Ev;      // Unity EventInstance name for Menu_Select_1 event that was created in FMOD

    [FMODUnity.EventRef]
    public string MenuSelect3 = "event:/Menu_Select_3";   // string reference to the FMOD-authored Event named "Menu_Select_3"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance MenuSelect3Ev;      // Unity EventInstance name for Menu_Select_1 event that was created in FMOD

    void Start()
    {

        quitMenu = quitMenu.GetComponent<Canvas>();
        startText = startText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        quitMenu.enabled = false;
        restart = restart.GetComponent<Button>();
        mainMenu = mainMenu.GetComponent<Button>();
        isDead = false;
        GameOverMenu.enabled = false;

    }

    public void ExitPress()
    {

        quitMenu.enabled = true;
        startText.enabled = false;
        exitText.enabled = false;

    }

    public void NoPress()
    {

        quitMenu.enabled = false;
        startText.enabled = true;
        exitText.enabled = true;

    }



    public void StartLevel()
    {

        SceneManager.LoadScene(1);

    }

    public void StartLavaMenu()
    {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect1Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect1);    // EventInstance is linked to the Event 
        MenuSelect1Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(0);

    }

    public void StartLavaLevel()
    {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect3Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect3);    // EventInstance is linked to the Event 
        MenuSelect3Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(3);

    }

    public void StartIceMenu()
    {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect1Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect1);    // EventInstance is linked to the Event 
        MenuSelect1Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(5);

    }

    public void StartIceLevel()
    {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect3Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect3);    // EventInstance is linked to the Event 
        MenuSelect3Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(6);

    }

    public void StartJungleMenu()
    {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect1Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect1);    // EventInstance is linked to the Event 
        MenuSelect1Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(4);

    }

    public void StartJungleLevel()
    {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect3Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect3);    // EventInstance is linked to the Event 
        MenuSelect3Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(2);

    }

    public void ExitGame()
    {

        Application.Quit();

    }

    public void RestartLevel() {

        SceneManager.LoadScene(1);

    }

    public void MainMenuAccess() {

        // These lines should be pasted into the section where you want to cue/play the sound 
        MenuSelect2Ev = FMODUnity.RuntimeManager.CreateInstance(MenuSelect2);    // EventInstance is linked to the Event 
        MenuSelect2Ev.start();                                            // FINALLY... Play the sound!!

        SceneManager.LoadScene(1);

    }

    public void GameOverScreen() {

        if(isDead == true)
        {

            GameOverMenu.enabled = true;

        }

    }

}
