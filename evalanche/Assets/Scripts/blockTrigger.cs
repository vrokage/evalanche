﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockTrigger : MonoBehaviour {

    public PlayerMovement player;
    public Canvas GameOverMenu;

    [FMODUnity.EventRef]
    public string death = "event:/Death";   // string reference to the FMOD-authored Event named "Death"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance deathEv;      // Unity EventInstance name for Death event that was created in FMOD

    void Start() {

        GameOverMenu.enabled = false;
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {


        if (other.gameObject.CompareTag("block") && other.gameObject.GetComponent <Rigidbody2D>().velocity.y < -.1 && player.grounded == true)
        {
            // These lines should be pasted into the section where you want to cue/play the sound 
            deathEv = FMODUnity.RuntimeManager.CreateInstance(death);    // EventInstance is linked to the Event 
            deathEv.start();                                            // FINALLY... Play the sound!!

            GameOverMenu.enabled = true;
            Destroy(transform.parent.gameObject);
            

        }

    }


}
