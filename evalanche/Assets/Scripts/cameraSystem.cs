﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraSystem : MonoBehaviour {

    private GameObject player;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    Vector3 offset;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        Vector3 cameraPos = transform.position;
        cameraPos.y = player.transform.position.y + offset.y;
        gameObject.transform.position = cameraPos;
	}
}
